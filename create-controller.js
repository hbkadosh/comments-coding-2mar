// create-controller.js
// match with ng-app at index.html
(function(){
    angular
        .module("WEDCOMMENT")  
        .controller("CreateCtrl", CreateCtrl);

    // inject dependencies into Controller
    CreateCtrl.$inject = ['$window', 'CommentService' ]; // CommentService must match in Services js file

    function CreateCtrl($window, CommentService){
        
        var vm = this;

        vm.comment = {
            date: new Date(),
            user: "",
            img: "",
            txt: ""
        };

        vm.status = {
            message: "",
            code: ""
        };

        vm.create = create; // to be callable by the View by the 'vm' as create()

        function create(){ // triggered by client - must be binded to VM as create()
            console.log("CreateCtrl - create() >> ");
            console.log("CreateCtrl - crate() >> %s",  JSON.stringify(vm.comment));

            CommentService
                .insertComment(vm.comment)   
                .then(function(result){
                    console.log("CreateCtrl create() INSERT SUCCESS >> %s", result.get({plain:true}));
                    showComment(vm.comment);
                    // $window.location.assign('/app/ctrl-create/thanks.html');

                    // res
                    //     .status(200)
                    //     .json(comment);
                }).catch(function(err){
                    console.log("CreateCtrl create() INSERT ERROR >> %s", JSON.stringify(err));
                    vm.status.message = err.data.name;
                    vm.status.code = err.data.parent;  // .errno;
                    console.log("CreateCtrl create() INSERT ERROR message >> %s", err.data.name);
                    console.log("CreateCtrl create() INSERT ERROR code >> %s", err.data.parent);

                    // res
                    //     .status(500)
                    //     .json(err);
                })

            CommentService
                .showComment(vm.comment)   
                .then(function(allComments){
                    console.log("CreateCtrl create() SHOW SUCCESS >> %s", result.get({plain:true}));
                    // show allComments in reverse order (LIFO) ?
                    var temp = allComments;
                    allComments = temp.reverse();
                    commentCtrl.comments = allComments;
                    // $window.location.assign('/app/ctrl-create/thanks.html');

                    // res
                    //     .status(200)
                    //     .json(comment);
                }).catch(function(err){
                    console.log("CreateCtrl create() SHOW ERROR >> %s", JSON.stringify(err));
                    vm.status.message = err.data.name;
                    vm.status.code = err.data.parent;  // .errno;
                    console.log("CreateCtrl create() SHOW ERROR message >> %s", err.data.name);
                    console.log("CreateCtrl create() SHOW ERROR code >> %s", err.data.parent);

                // old coding - CommentSvc ($http) to send data to server
                // commentCtrl.placeComment = function() {
                //     console.log("Comment CTRL: inside commentCtrl.placeComment...");
                //     commentCtrl.timestamp = (new Date()).toString(); // set time to comment
                //     commentCtrl.addArr(); // push object into array
                //     CommentSvc.placeComment(commentQueryObject(commentCtrl))
                //         .then(function() {
                //             commentCtrl.showComment();
                //             initForm(commentCtrl);
                //         });
                // }

                // commentCtrl.showComment = function() {
                //     console.log("Comment CTRL: inside commentCtrl.showComment...");
                //     commentCtrl.comments = [];
                //     CommentSvc.showComment()
                //             .then(function(allComments) {
                //                 // show allComments in reverse order (LIFO) ?
                //                 var temp = allComments;
                //                 allComments = temp.reverse();
                //                 commentCtrl.comments = allComments;
                //                 console.log("Comment CTRL: ", allComments);
                //     })            
                })
        }
    }
    
}) ();






