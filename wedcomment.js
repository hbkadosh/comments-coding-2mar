// mdemo1.js

// a) define Models
// b) setup Express
// c) estb Connection
// d) associate Models

// 1)
var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var Sequelize = require("sequelize");
var mysql = require("mysql");
var mongoose = require("mongoose");

// 2)
const NODE_PORT = process.env.NODE_PORT || 3000;
const CLIENT_FOLDER = path.join(__dirname, '../client');
const MSG_FOLDER = path.join(CLIENT_FOLDER, 'assets/messages');
const MYSQL_USERNAME = "hb";
const MYSQL_PASSWORD = "root777";
// const MONGODB_USERNAME = "";
// const MONGODB_PASSWORD = "";
const API_DEPT_ENDPT = "/api/departments";

// 3)
var app = express();


var sequelize = new Sequelize(
    'commentsdb',
    MYSQL_USERNAME,
    MYSQL_PASSWORD,
    {
        host: 'localhost',
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 5,     // sequelize is Stateful - min. network connection overhead
            min: 0,
            idle: 10000
        }
    }
)

sequelize.authenticate()
    .then(function() {
        console.log("DB authenticated");
    })
    .catch(function(err) {
        console.log(err);
    });

// import own App JS modules (the database models)
var Comment = require('./models/comment')(sequelize, Sequelize); // import own App modules
var Image = require('./models/image')(sequelize, Sequelize); // import own App modules


sequelize.query("SELECT * FROM commenttbl where c_image = '222.jpg'", { type: Sequelize.QueryTypes.SELECT})
    .then(function(results) {        
        console.log("APP SVR Sequelize Query >> ", results);

            // show allComments in reverse order (LIFO) ?
            var temp = results;
            results = temp.reverse();
            // commentCtrl.comments = results;
        console.log("APP SVR Sequelize Query LIFO >> ", results);
    })
    .catch(function(err) {
        console.log(err);
    });

Comment
    .findOne({    // .findAll() always returns an array
    })
    .then(function(result) {
        console.log("APP SVR findOne() >> ", result.dataValues.c_date);
        console.log("APP SVR findOne() >> ", result.dataValues.c_user);
        console.log("APP SVR findOne() >> ", result.dataValues.c_image);
        console.log("APP SVR findOne() >> ", result.dataValues.c_text);
    })
    .catch(function(err) {
        console.log(err);
    });
// MySQL: declare & map data record associations/relationships
// Department.hasMany(Manager, { foreignKey: 'dept_no'});
// Manager.hasOne(Employee, { foreignKey: 'emp_no'});

// 3)
app.use(express.static(CLIENT_FOLDER));

app.use(bodyParser.json());
// accept ONLY json - usually for API dev
// app.use(bodyParser.urlencoded({extended: false})); 

// 4) routes
/**
 * GET /api/departments
 * POST /api/departments
 * DELETE /api/departments/:dept_no/manager/:emp_no
 * PUT /api/departments/:dept_no
 * GET /api/static/departments -> retrieve static data of dept listing
 */

app.post("/comments1", function(req, res) {
    console.log("In comments");
       console.log("POST >> %s", JSON.stringify(req.body.cmt));
    res.status(200).send("In comments");
});

// create a single comment into MySQL
app.post("/comments", function(req, res){
    console.log("POST >> %s", JSON.stringify(req.body.cmt));

    Comment
        .create({
            c_date: new Date(req.body.cmt.date),
            c_user: req.body.cmt.user,
            c_image: req.body.cmt.img,
            c_text: req.body.cmt.txt
        })
        .then(function(commentrow){
            console.log("POST >> SUCCESS %s", commentrow);
            res
                .status(200)
                .json(commentrow);
        })
        .catch(function(){
            console.log("POST >> ERROR %s", err);
            res
                .status(500)
                .json(err);
        })
});


// route path - retrieve/show all comments
 app.get(API_DEPT_ENDPT, function(req, res){
     console.log(req.query.searchString);
     var searchString  = req.query.searchString
     
      Comment
         .findAll({
         })
         .then(function(commentrow){
            console.log("APP SVR findAll() >> ", commentrow);  // .dataValues.c_date);
            res
                 .status(200)
                 .json(commentrow);
         })
         .catch(function(err){
             res
                 .status(500)
                 .json(err);
         });   
 });


// Wild card handling
app.use(function(req, res) {
    console.log("END >> 404...");
    res.status(404).sendFile(path.join(MSG_FOLDER, "404.html"));
});

// Error handling
app.use(function(err, req, res, next) {
    console.log("ERROR >> 500...", err);
    res.status(501).sendFile(path.join(MSG_FOLDER, "501.html"));
});


// Server start
app.listen(NODE_PORT, function(){
    console.log("WEDCOMMENT Server started: port %d on %s", NODE_PORT, new Date());
});

