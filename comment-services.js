// comment-services.js

// TODO: Send and retrieve information from server via Angular $http; modularize via services
// TODO: 1. Define a service that would hold logic related to a comment
// TODO: 2. Inject the dependency needed to communicate with the server
// TODO: 3. Create a function that accepts comment-creation information and sends this info to server.
// TODO: 3a Name the function insertComment
// TODO: 3b Return promise object to calling controller
// TODO: 3c Expose this function.
// TODO: 11 Create a function called getAllComments that retrieves all comments information from server using HTTP GET.
// TODO: 11 This function returns a promise object.


// TODO: 1.1 Create an IIFE then call your angular module and attach your service to it. Name the service CommentService
(function () {
    // Attaches CommentService service to the MDEMO1 module (MEAN Demo 1)
    angular
        .module("WEDCOMMENT")
        .service("CommentService", CommentService);

    // TODO: 2.1 Inject $http. We will use this built-in service to communicate with the server
    CommentService.$inject = ['$http'];

    // TODO: 1.2 Declare CommentService
    // TODO: 2.2 Accept the injected dependency as a parameter.

    function CommentService($http) {

        var service = this;
        // TODO: 1.3 Assign this object to a variable named service
        // TODO: 3.3 Expose insertComment
        service.insertComment = insertComment;
        service.showComment = showComment;
        
        function insertComment(comment) {
            console.log("SVC insert() >> %s", comment);
            return $http({
                method: 'POST',
                url: 'comments',
                data: { cmt: comment } // body of response messageS 
            });
        }

        function showComment(comment) {
            console.log("SVC show() >> %s", comment);
            return $http({
                method: 'GET',
                url: 'comments',
                data: { cmt: comment } // body of response messageS 
            });
        }
        // old coding
        // commentSvc.placeComment = function(commentDetail) {
        //     console.log("Comment SVC: placeComment()---");
        //     console.log("Comment SVC: commentDetail: ", commentDetail);
        //     return ($http.get("/placeComment", {
        //         params: commentDetail  
        //     }));
        // }

        // commentSvc.showComment = function() {
        //     console.log("Comment SVC: showComment()...");
        //     return ($http.get("/showComment")
        //         .then(function(result) {
        //             console.log("Comment SVC: >>> Results: %s", result);
        //             return (result.data);
        //         }));
        // }
        
    }

        // TODO: 11.1 Declare retrieveComment; retrieveComment must be a private function; retrieveComment retrieves all comment data
        // TODO: 11.1 from server and returns a promise object// retrieveComment retrieves comments information from the server via HTTP GET.

}) ();

