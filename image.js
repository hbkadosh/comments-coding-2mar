// image.js
// map to MySQL data model - to be required into server App JS

module.exports = function(connection, Sequelize){
    var Images = connection.define("imagetbl", {
        image_id: {
            type: Sequelize.STRING,
            allowNull: false,
            primaryKey: true
        },
    }, {
            tableName: "imagetbl",
            timestamps: false
        }
    );
    console.log("Models-Image >> ", Images);
    return Images;
};


